import React, { Component } from 'react';
//import logo from './logo.svg';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Container,Row, Col,} from 'reactstrap';
import './App.css';
import ReactStopwatch from 'react-stopwatch';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  render() {
    return (
      <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '100vh'}}>
         
           
      <Button color="promary" onClick={this.toggle}>chronometre</Button>
      
      <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>le temp écoulé</ModalHeader>
  
        <ModalFooter>
        <ReactStopwatch
            seconds={0}
            minutes={0}
            hours={0}
            limit="0:01:00"
            onChange={({ hours, minutes, seconds }) => {
      // do something
                if(seconds ==10){
                  this.toggle();
                }
              }}
            //onCallback={() => console.log('Finish')}
            render={({ formatted, hours, minutes, seconds }) => {
            return (
            <div>
              <p>
                Formatted: { formatted }
              </p>
            </div>
      );
    }}
   />
          <Button color="primary" onClick={this.toggle}>Stop</Button>{' '}
        </ModalFooter>
      </Modal>
    
    </div>
    );
  }
}

export default App;
